package com.humworks.dcs.service;

public interface CommonService {
	
	Integer getPatternFromUrl(String pattern);
	
	Integer getIdFromUrl(String url);
	
	String getNameFromUrl(String url);

}
