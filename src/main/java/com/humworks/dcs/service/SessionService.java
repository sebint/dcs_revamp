package com.humworks.dcs.service;

public interface SessionService {

	Integer getActiveUid();
	
	String getActiveUsername();
}
